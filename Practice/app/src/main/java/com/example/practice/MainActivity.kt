package com.example.practice

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buttonHealthInfo = findViewById(R.id.button_health_info) as ImageButton;

        buttonHealthInfo.setOnClickListener {
            // your code to perform when the user clicks on the button
            val intent = Intent(this, DisplayMessageActivity::class.java);
            startActivity(intent);
        }
    }

}