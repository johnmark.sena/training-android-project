package com.example.practice

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class DisplayMessageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_message)

        val buttonConfimation = findViewById<Button>(R.id.button_confirmation);

        buttonConfimation.setOnClickListener {
            // your code to perform when the user clicks on the button
            val i = Intent(this, ConfirmationActivity::class.java);
            startActivity(i);
        }

    }


}