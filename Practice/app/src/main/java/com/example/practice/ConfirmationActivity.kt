package com.example.practice

import android.content.Intent
import android.os.Bundle
import android.provider.AlarmClock
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class ConfirmationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)

        val buttonSave = findViewById<Button>(R.id.buttonSave);
        val buttonReturn = findViewById<Button>(R.id.buttonReturn);

        buttonSave.setOnClickListener {
            // your code to perform when the user clicks on the button
            val i = Intent(this, SuccessScreenActivity::class.java);
            startActivity(i);
        }

        buttonReturn.setOnClickListener {
            // your code to perform when the user clicks on the button
            val i = Intent(this, DisplayMessageActivity::class.java);
            startActivity(i);
        }

        // Get the Intent that started this activity and extract the string
    }
}