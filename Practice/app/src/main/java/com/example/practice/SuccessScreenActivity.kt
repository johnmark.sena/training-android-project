package com.example.practice

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class SuccessScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success_screen)

        val buttonReturn = findViewById<Button>(R.id.button_home);

        buttonReturn.setOnClickListener {
            // your code to perform when the user clicks on the button
            val i = Intent(this, MainActivity::class.java);
            startActivity(i);
        }
    }
}